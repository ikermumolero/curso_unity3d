﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class Player : MonoBehaviour
{
    private float Speed = 7;
    private float SetSpeed = 7;
    private float DashSpeed = 14;
    private float Jump = 7;
    private float Move;
    private Rigidbody2D Rig;
    private bool FRight = true;
    private bool Ground;
    private float CRadius = .25f;
    private int EJumpsValue = 1;
    private bool TouchF;
    private bool Sliding;
    private float SSpeed = 1;
    private bool WJump;
    private float xWForce = 2;
    private float yWForce = 6;
    private float WJTime = 0.05f;
    private float DForce = 100;
    private float SDTime = .5f;
    private float CDT;
    private float DDirection;
    [Header ("Publics")]
    public bool Dash;
    private int EDashValue = 1;
    [Header ("Layers")]
    public LayerMask WGround;
    [Header ("Checks")]
    public Transform GCheck;
    public Transform FCheck;
    [Header ("UI")]
    public int EDash;
    public int EJumps;
    [Header("Particles")]
    public ParticleSystem WPart;
    public ParticleSystem JPart;
    public ParticleSystem WJPartR;
    public ParticleSystem WJPartL;
    public ParticleSystem DPart;
    private Vector3 SpawnPoint01 = new Vector2(14.5f, 5.5f);
    private Vector3 SpawnPoint02 = new Vector2(54.5f, 25.5f);
    private Vector3 SpawnPoint02B = new Vector2(108.5f, 32.5f);
    private Vector3 SpawnPoint03 = new Vector2(126.5f, 32.5f);
    private Vector3 SpawnPoint04 = new Vector2(140.5f, 32.5f);
    private Vector3 SpawnPoint05 = new Vector2(166.5f, 42.5f);
    [Header("Anim")]
    public Animator PlayerAnim;
    void Start()
    {
        EJumps = EJumpsValue;
        EDash = EDashValue;
        Rig = GetComponent<Rigidbody2D>();
    }
    private void FixedUpdate()
    {
        Ground = Physics2D.OverlapCircle(GCheck.position, CRadius, WGround);
        Move = Input.GetAxis("Horizontal");
        Rig.velocity = new Vector2(Move * Speed, Rig.velocity.y);
        if (FRight == false && Move > 0)
        {
            Flip();
        }else if (FRight == true && Move < 0)
        {
            Flip();
        }
    }
    private void Flip()
    {
        FRight = !FRight;
        Vector3 Scaler = transform.localScale;
        Scaler.x *= -1;
        transform.localScale = Scaler;
        if (Ground)
            WDust();
    }
    void Update()
    {
        if (Ground)
        {
            EJumps = EJumpsValue;
            EDash = EDashValue;
        }
        if (Input.GetKeyDown(KeyCode.W) && EJumps > 0 && Sliding == false)
        {
            Rig.velocity = Vector2.up * Jump;
            EJumps--;
            JDust();
        } else if (Input.GetKeyDown(KeyCode.W) && EJumps == 0 && Ground == true)
        {
            Rig.velocity = Vector2.up * Jump;
        }
        TouchF = Physics2D.OverlapCircle(FCheck.position, CRadius, WGround);
        if (TouchF == true && Ground == false && Move != 0)
        {
            Sliding = true;
            if (Move > 0) WJDustR();
            if (Move < 0) WJDustL();
        }
        else
        {
            Sliding = false;
        }
        if (Sliding)
        {
            Rig.velocity = new Vector2(Rig.velocity.x, Mathf.Clamp(Rig.velocity.y, -SSpeed, float.MaxValue));
        }
        if (Input.GetKeyDown(KeyCode.W) && Sliding == true)
        {
            WJump = true;
            Invoke("SetWJToFalse", WJTime);
            if (Move > 0) WJDustR();
            if (Move < 0) WJDustL();
        }
        if (WJump == true)
        {
            Rig.velocity = new Vector2(xWForce * Move, yWForce);
        }
        if (Input.GetKeyDown(KeyCode.LeftShift) && !Ground && Move != 0 && EDash > 0)
        {
            Dash = true;
            CDT = SDTime;
            Rig.velocity = Vector2.zero;
            DDirection = (int)Move;
            EDash--;
            DDust();
        }
        if (Dash)
        {
            Rig.velocity = transform.right * DDirection * DForce;
            Speed = DashSpeed;
            CDT -= Time.deltaTime;
            if (CDT <= 0)
            {
                Dash =  false;
            }
        }
        else
        {
            Speed = SetSpeed;
        }
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("DeathZone01")) this.gameObject.transform.position = SpawnPoint01;
        if (other.gameObject.CompareTag("DeathZone02")) this.gameObject.transform.position = SpawnPoint02;
        if (other.gameObject.CompareTag("DeathZone02B")) this.gameObject.transform.position = SpawnPoint02B;
        if (other.gameObject.CompareTag("DeathZone03")) this.gameObject.transform.position = SpawnPoint03;
        if (other.gameObject.CompareTag("DeathZone04")) this.gameObject.transform.position = SpawnPoint04;
        if (other.gameObject.CompareTag("DeathZone05")) this.gameObject.transform.position = SpawnPoint05;
    }
    void SetWJToFalse()
    {
        WJump = false;
    }
    void WDust()
    {
        WPart.Play();
    }
    void JDust()
    {
        JPart.Play();
    }
    void WJDustR()
    {
        WJPartR.Play();
    }
    void WJDustL()
    {
        WJPartL.Play();
    }
    void DDust()
    {
        DPart.Play();
    }
}